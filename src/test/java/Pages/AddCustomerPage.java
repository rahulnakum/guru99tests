package Pages;

import Pages.BasePage;
import org.openqa.selenium.By;

public class AddCustomerPage extends BasePage {

    public By lblHeaderAddCustomerPage = By.xpath("//h1[text()='Add Customer']");
    public By inputFirstName = By.id("fname");
    public By labErrorForFirstName = By.xpath("//*[@id='fname']/following-sibling::*");
}
