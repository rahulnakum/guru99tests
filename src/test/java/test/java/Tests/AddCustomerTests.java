package test.java.Tests;

import Pages.AddCustomerPage;
import Utilities.BaseTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class AddCustomerTests extends BaseTest {

    AddCustomerPage addCustomerPage = new AddCustomerPage();

    @DataProvider(name = "AddCustomerData")
    public static Object[][] AddCustomerData() {
        return new Object[][] {
                { "rahul", "nakum" },
                { "zxcvbnmasd,", "lname1" },
                { "abcd", "qwertyuiop" }
        };
    }

    @BeforeClass
    public void setup() throws IOException {
        driver.findElement(addCustomerPage.linkAddCustomer).click();
    }

    @Test(priority = 1)
    public void validateAddCustomerPageHeader(){
        String currentUrl = driver.getCurrentUrl();
        System.out.println("actual currentUrl : " + currentUrl);
        String expectedUrl = "http://demo.guru99.com/telecom/addcustomer.php";
        //validate url
        Assert.assertEquals(currentUrl, expectedUrl, "Url did not match");

        //validate if header is present
        int countHeader = driver.findElements(addCustomerPage.labErrorForFirstName).size();
        Assert.assertEquals(countHeader, 1, "Header is not present");
    }

    @Test(priority = 2, dependsOnMethods = {"validateAddCustomerPageHeader"})
    public void validateErrorIfFormIsNotFilled() throws InterruptedException {
        //click on submit button
        driver.findElement(addCustomerPage.btnSubmit).click();
        String actualMessage = driver.switchTo().alert().getText();
        System.out.println("actual actualMessage : " + actualMessage);
        String expectedAlertMessage = "please fill all fields";

        //close alert
        driver.switchTo().alert().dismiss();
        Thread.sleep(5000);
        Assert.assertEquals(actualMessage,expectedAlertMessage, "Alert message validation failed");
    }

    @Test(priority = 3)
    public void validateNumbersInNameResturnsError(){
        driver.findElement(By.id("fname")).sendKeys("123");
        String actualMessage = driver.findElement(addCustomerPage.labErrorForFirstName).getText();
        System.out.println("actualMessage : " + actualMessage);
        String expectedMessage = "Numbers are not allowed";

        Assert.assertEquals(actualMessage, expectedMessage, "Message did not match");
    }

    @Test(priority = 4, dataProvider = "AddCustomerData")
    public void addCustomerWithAllValidValues1(String firstname, String lastName){
        driver.findElement(By.xpath("//label[contains(text(),'Done')]")).click();
        driver.findElement(addCustomerPage.inputFirstName).sendKeys(firstname);
        driver.findElement(By.id("lname")).sendKeys(lastName);
        driver.findElement(By.id("email")).sendKeys("test@test.com");
        driver.findElement(By.name("addr")).sendKeys("address1");
        driver.findElement(By.id("telephoneno")).sendKeys("0123456789");
        driver.findElement(addCustomerPage.btnSubmit).click();

        //validate if the message is displayed on the page
        int countActualmessgae = driver.findElements(By.xpath("//h1[contains(text(),'Access Details to Guru99 Telecom')]")).size();
        Assert.assertEquals(countActualmessgae,1);
        //Capture customer Id
        String customerId = driver.findElement(By.xpath("//table//tr[1]//td[2]")).getText();
        //Assert.assertTrue(!customerId.isEmpty());
        driver.get("http://demo.guru99.com/telecom/addcustomer.php");
    }
}
