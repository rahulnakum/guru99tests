package test.java.Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CrossBrowserTest {

    WebDriver driver;
    Properties prop;

    @BeforeClass
    public void setup() throws IOException {
        //read properties
        prop = new Properties();
        prop.load(new FileInputStream((System.getProperty("user.dir") + "\\src\\test\\resouces\\project.properties")));

        //setup cromedriver
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") +prop.getProperty("chromeDriverPath"));

        //set Firefox driver
        System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir") +prop.getProperty("gecoDriverPath"));

        //IE
        System.setProperty("webdriver.ie.driver",System.getProperty("user.dir") +prop.getProperty("IEDriverPath"));

    }

    @Test
    public void chromeTest(){
        driver = new ChromeDriver();
        driver.get("https://www.google.co.in");
    }

    @Test
    public  void fireFoxTest(){
        driver = new FirefoxDriver();
        driver.get("https://www.google.co.in");
    }

    @Test
    public  void IE_Test(){
        driver = new InternetExplorerDriver();
        driver.get("https://www.google.co.in");
    }
}
