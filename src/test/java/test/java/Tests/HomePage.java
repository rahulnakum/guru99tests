package test.java.Tests;

import Utilities.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.mustache.BaseChunk;

import java.io.*;
import java.util.Properties;

public class HomePage extends BaseTest {

    @Test(priority = 1)
    public void validateIfGuru99TelecomLinkPresent(){
        int countHomePageLinks =  driver.findElements(By.linkText("Guru99 telecom")).size();
        Assert.assertEquals(countHomePageLinks, 1, "Homepage link is not displayed");
    }

    @Test(priority = 2)
    public void validateIfAddCustomerLinkPresent(){
        int countAddCustomerLinks =  driver.findElements(By.linkText("Add Customer")).size();
        Assert.assertEquals(countAddCustomerLinks, 1);
    }

}
